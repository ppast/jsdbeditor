// ==ClosureCompiler==
// @compilation_level SIMPLE_OPTIMIZATIONS
// @output_file_name default.js
// @language_out ECMASCRIPT_2017
// ==/ClosureCompiler==




window['ppast'] = window['ppast'] || {};
ppast.JsDbEditor = {};




// classe d'interface avec backoffice
ppast.JsDbEditor.MysqlInterface = class extends nettools.PdoServerInterface 
{
	/**
	 * Constructor of PdoServerInterface
	 *
	 * @param bool useCompression Is compression enabled for xmlhttp responses ? Defaults to false
	 */
	constructor(ns, cmd, session, routage, useCompression = false)
	{
		super(useCompression);
		
		this.namespace = ns;
		this.command = cmd;
		this.session = session;
		this.routage = routage;
	}

	
	
	/**
	 * Send a SQL query described in payload object litteral
	 *
	 * @param object payload
	 * @param bool expectResult
	 * @return Promise Returns a promise resolved if query OK ; if payload.noResponseBody=false, the Promise is resolved with SELECT response
	 */
	send(payload)
	{
		// ajouter commande 
		payload.__namespace__ = this.namespace;
		payload.__command__ = this.command;
		payload.__session__ = this.session;


		return nettools.jscore.SecureRequestHelper.sendXmlHTTPRequestPromise(this.routage, payload);
	}
}







ppast.JsDbEditor.Parametres = {
	/**
	 * Initialiser l'édition des tables de paramètres
	 *
	 * @param string[] tables Liste des tables pouvant être éditées
	 * @param string id Attribut ID d'un élément HTML qui recevra l'interface utilisateur
	 * @param string ns Namespace App_Framework pour atteindre le namespace de l'interface serveur PHP
	 * @param string cmd Commande désignant le script d'interface serveur PHP 
	 * @param string session Session actuelle
	 * @param string routage Chemin vers le script principal de l'application
	 */
	init : function(tables, id, ns, cmd, session, routage)
	{
		var dbeditor = new nettools.DatabaseEditor(
				// tables list
				tables,

				// node
				document.getElementById(id),

				// database of type SQL.Database
				nettools.MysqlPdoTableEditor.bind(null, new ppast.JsDbEditor.MysqlInterface(ns, cmd, session, routage)),

				// options
				{
					// underlying jsGridEditor options
					gridEditorOptions : {
						dialog : new nettools.jsGridEditor.UIDesktopDialog(),
					}					
				}
			);	
	}
};









ppast.JsDbEditor.Configuration = {
	/**
	 * Initialiser l'édition de la table de configuration ; la table doit comporter au moins 3 colonnes : 'id', 'metadata', 'valeur'
	 *
	 * @param string table Nom de la table de configuration
	 * @param string id Attribut ID d'un élément HTML qui recevra l'interface utilisateur
	 * @param object mysqlIntf Littéral objet définissant les propriétés 'namespace', 'command', 'session', 'routage', 'useCompression', destinées à initialiser l'objet ppast.JsDbEditor.MysqlInterface
	 * @param string[] Liste des colonnes requises (au moins 'id')
	 * @param string orderBy Clause 'order by' SQL pour lister les lignes
	 * @param object defautValues Littéral objet avec les valeurs par défaut pour la création d'une nouvelle ligne
	 * @param int lineLength Longueur d'affichage avant coupure pour les valeurs de type `html` et `longtext`
	 */
	init : function(table, id, mysqlIntf, required, orderBy = 'id', defaultValues = {}, lineLength = 100)
	{
		var grid = new nettools.DbConfigEditor(
				// table config
				table,

				// node
				document.getElementById(id),

				// options
				{
					metadataColumn : 'metadata',
					valueColumn : 'valeur',
					primaryKeyColumn : 'id',
					lineLength : lineLength,
					requiredColumns : required
				},

				// SQLTableEditor class
				nettools.MysqlPdoTableEditor.bind(null, new ppast.JsDbEditor.MysqlInterface(mysqlIntf.namespace, mysqlIntf.command, mysqlIntf.session, mysqlIntf.routage, mysqlIntf.useCompression ? true : false)),

				// SQLTableEditor options
				{
					orderBy : orderBy,
					
					// jsGridEditor options
					gridEditorOptions :
					{
						dialog : new nettools.jsGridEditor.UIDesktopDialog(),
						defaultValues : defaultValues
					}
				}
			);


		grid.setup().catch(function(e){
			alert(e.message ? e.message : e);
		});
	}
};