<?php

namespace Ppast\JsDbEditor;






class JsDbEditorIntf extends \Nettools\JsDbEditor\Pdo\XmlHttpResponseIntf
{
	/**
	 * Test if a row can be deleted
	 *
	 * @param object $body Request body as json object with 'tableName', 'rowNumber', 'row' properties 
	 * @return bool
	 */
	function allowDelete($body)
	{
		// get first column name = foreign key ; get all properties and their values (as associative array), then get only first value : this is the foreign key value
		$kv = array_values(get_object_vars($body->row))[0];
		
		// test foreign key is used somewhere or not
		$r = $this->pdo->pdo_foreignkeys($body->tableName, $kv);
		
		return $r['status'];
	}
	
	
	
	/**
	 * Executer requête via l'interface PHP
	 *
	 * @param PdoHelper $pdo
	 */
	static function run(\Nettools\Core\Helpers\PdoHelper $pdo)
	{
		$intf = new JsDbEditorIntf($pdo);
		$intf->execute();
	}
} 


?>